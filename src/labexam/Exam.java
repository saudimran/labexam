package labexam;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class Exam {
	WebDriver driver = new FirefoxDriver();
	@BeforeTest
	   public void launchapp() {
	      // Puts an Implicit wait, Will wait for 10 seconds before throwing exception
		      driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		      
		      // Launch website
		      driver.navigate().to("https://www.makemytrip.com/");
		      driver.manage().window().maximize();
	   }
	
	@Test
	public void Search()
	{
		driver.findElement(By.xpath(".//*[@id='header_tab_flights']")).click();
		driver.findElement(By.xpath(".//*[@id='hp-widget__sfrom']")).sendKeys("Mumbai (BOM)");
		driver.findElement(By.xpath(".//*[@id='hp-widget__sTo']")).sendKeys("Bangalore (BLR)");
		driver.findElement(By.xpath(".//*[@id='hp-widget__depart']")).sendKeys("9 Nov, Thu");
		
		driver.findElement(By.xpath(".//*[@id='searchBtn']")).click();
		
	}
	@AfterTest
	   public void terminatetest() throws InterruptedException 
	   {

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.close();
	   }


}
